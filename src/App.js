import {Fragment} from 'react';
import {Container} from 'react-bootstrap';

import './App.css';

// components
import AppNavbar from './components/AppNavbar';

// pages
import Home from './pages/Home';
import Courses from './pages/Courses';
import Register from './pages/Register';
import Login from './pages/Login';

function App() {

  return(
    <Fragment>
      <AppNavbar />
      <Login />
    </Fragment>
  )

  // return(
  //   <Fragment>
  //     <AppNavbar />
  //     <Container>
  //       <Home />
  //       <Courses />
  //       <Register />
  //     </Container>
  //   </Fragment>
  // )
}

export default App;
