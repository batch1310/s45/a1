import {Nav, Navbar} from 'react-bootstrap';

export default function AppNavbar(){
	
	return(
		<Navbar bg="light" expand="lg" className="px-4">
			<Navbar.Brand href="#">React Booking</Navbar.Brand>
			<Navbar.Toggle aria-controls="basic-navbar-nav" />

			<Navbar.Collapse id="basic-navbar-nav">
				<Nav className="me-auto">
					<Nav.Link href="#home">Home</Nav.Link>
        			<Nav.Link href="#courses">Courses</Nav.Link>
				</Nav>
			</Navbar.Collapse>
		</Navbar>
	)
}

