import {Card, Col, Row, Container} from 'react-bootstrap';

export default function Highlights(){
	return(

		<Container fluid className="p-1 my-2">
			<Row>
				{/* card 1*/}
				<Col xs={12} md={4}>
					<Card className="cardHighlights">
					  <Card.Body>
					    <Card.Title>Learn from Home</Card.Title>
					    <Card.Text>
					    	Lorem, ipsum dolor sit amet consectetur adipisicing, elit. Expedita nobis sequi laudantium possimus temporibus, tenetur? Facilis, cupiditate possimus ipsam, quae unde eveniet voluptate, illum sunt commodi corporis, at aperiam error.
					    </Card.Text>
					  </Card.Body>
					</Card>
				</Col>

				{/* card 2*/}
				<Col xs={12} md={4}>
					<Card className="cardHighlights">
					  <Card.Body>
					    <Card.Title>Study Now Pay Later</Card.Title>
					    <Card.Text>
					    	Lorem, ipsum dolor sit amet consectetur adipisicing, elit. Expedita nobis sequi laudantium possimus temporibus, tenetur? Facilis, cupiditate possimus ipsam, quae unde eveniet voluptate, illum sunt commodi corporis, at aperiam error.
					    </Card.Text>
					  </Card.Body>
					</Card>
				</Col>

				{/* card 3*/}
				<Col xs={12} md={4}>
					<Card className="cardHighlights">
					  <Card.Body>
					    <Card.Title>Be Part of Our Community</Card.Title>
					    <Card.Text>
					    	Lorem, ipsum dolor sit amet consectetur adipisicing, elit. Expedita nobis sequi laudantium possimus temporibus, tenetur? Facilis, cupiditate possimus ipsam, quae unde eveniet voluptate, illum sunt commodi corporis, at aperiam error.
					    </Card.Text>
					  </Card.Body>
					</Card>
				</Col>
			</Row>
		</Container>
	)
}