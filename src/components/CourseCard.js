import {useState, useEffect} from 'react';
import {Card, Col, Row, Container} from 'react-bootstrap';
import PropTypes from 'prop-types';

export default function CourseCard({courseProp}){
	console.log(courseProp)

	const {name, description, price} = courseProp

	//Use the state hook for this component to be able to store it's state
	//States are used to keep track of information related to individual components
	/*
		Syntax:
			const [getter, setter] = useState(initialGetterValue)
	*/
	const [count, setCount] = useState(0)
	const [availableSeats, setAvailableSeat] = useState(10)
	console.log(useState(0))

	const [isDisabled, setIsDisabled] = useState(false)

	function enroll(){
		if(count > 9) {
			alert('No more seats available.');
			// document.getElementById("enrollBtn").disabled = false;
		} else {
			setCount(count + 1);
			setAvailableSeat(availableSeats - 1);
			console.log(availableSeats);
			console.log('Enrolees ' + count);

		}
	}

	// useEffect(cb(), [])
	useEffect( () => {

		if(count > 9) {
			setIsDisabled(true)
		}
	}, [count])



	return(
		<Container className="px-1">
			<Row>
				<Col xs={12} md={12} lg={12} className="mb-3">
					<Card className="cardHighlights">
					  <Card.Body>
					    <Card.Title>{name}</Card.Title>
					    <Card.Text><b>Description:</b> {description}</Card.Text>
					    <Card.Text><b>Price:</b> Php {price}</Card.Text>

					    <Card.Text><b>Enrollees:</b> {count} Enrollees</Card.Text>
					    <Card.Text><b>Available Seat/s:</b> {availableSeats} remaining.</Card.Text>
					    <button id="enrollBtn" className="btn btn-primary" onClick={enroll} disabled={isDisabled}>Enroll</button>
					  </Card.Body>
					</Card>
				</Col>
			</Row>
		</Container>
	)
}

//Validation: Check if the CourseCard component is getting the correct prop types
//PropTypes are used for validating information passed to a component and is a tool normally used to help developers ensure the correct information is passed from one component to the next.
CourseCard.propTypes = {
	//shape method => is used to check if a prop object confirms to a specific "shape"
	courseProp: PropTypes.shape({
		//define the properties and their expected types
		name: PropTypes.string.isRequired,
		description: PropTypes.string.isRequired,
		price: PropTypes.number.isRequired
	})
}


