import {Button} from 'react-bootstrap';

export default function Banner(){
	return(

		<div className="jumbotron py-5 px-2">
		  <h1>Welcome to React Booking App!</h1>
		  <p>Oppurtunities for everyone, everywhere!</p>
		  <Button variant="primary">Enroll Now!</Button>
		</div>

	)
}