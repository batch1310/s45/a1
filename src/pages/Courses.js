
import coursesData from './../data/courseData';

// components
import CourseCard from './../components/CourseCard';

export default function Courses(){
	
	console.log(coursesData)

	const courses = coursesData.map(course => {
		return(
			<CourseCard key={course.id} courseProp={course} />
		)
	})

	return(
		<>
			{courses}
		</>
	)
}