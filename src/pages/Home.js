
import {Fragment} from 'react';

import Banner from './../components/Banner';
import Highlights from './../components/Highlights';
// import CourseCard from './../components/CourseCard';

export default function Home(){

	return(
		<Fragment>
			<Banner />
			<Highlights />

			<h6 className="p-2 mt-4 courses__offered__tag mx-1">Courses Offered</h6>
			
			{/*<CourseCard />*/}
		</Fragment>
	)
}