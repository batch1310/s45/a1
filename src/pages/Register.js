import {useState, useEffect} from 'react';
import {Col, Row, Container, Form, Button} from 'react-bootstrap';

export default function Register(){

	const [email, setEmail] = useState("")
	const [password, setPsw] = useState("")
	const [confirmPassword, setCp] = useState("")
	const [isDisabled, setIsDisabled] = useState(true)

	console.log(email)
	console.log(password)
	console.log(confirmPassword)

	function registerUser(e){
		e.preventDefault()

		setEmail("")
		setPsw("")
		setCp("")

		alert("Thank you for registering to React Booking!")
	}


	/*
		Mini Activity
			Using useEffect, disable the button to prevent submitting. Allow button to submit if input fields are not empty and password should match
	*/
	useEffect( () => {
		if( (email !== "" && password !== "" && confirmPassword !== "") && ( password === confirmPassword) ){
			setIsDisabled(false)
		}
	}, [email, password, confirmPassword])


	return(
		<Container className="my-3 px-1">
			<Row>
				<Col>
					<Form className="border p-3" 
						  onSubmit={(e) => registerUser(e)}>

						{/*email*/}
						<Form.Group className="mb-2" controlId="formBasicEmail">
							<Form.Label>Email address</Form.Label>
							<Form.Control 
								type="email" 
								placeholder="Enter email" 
								value={email} 
								onChange={ (e) => {
									setEmail(e.target.value)
									//console.log(e.target.value)
								}}
							/>
						</Form.Group>

						{/*password*/}
						<Form.Group className="mb-2" controlId="formBasicPassword">
							<Form.Label>Password</Form.Label>
							<Form.Control 
								type="password" 
								placeholder="Password"
								value={password} 
								onChange={ (e) => {
									setPsw(e.target.value)
								}}
							/>
						</Form.Group>

						{/*confirm password*/}
						<Form.Group className="mb-2" controlId="confirmPassword">
							<Form.Label>Confirm Password</Form.Label>
							<Form.Control 
								type="password" 
								placeholder="Confirm Password"
								value={confirmPassword} 
								onChange={ (e) => {
									setCp(e.target.value)
								}}
							/>
						</Form.Group>

						{/*submit button*/}
						<Button variant="primary" type="submit" disabled={isDisabled}>Submit</Button>
					</Form>
				</Col>
			</Row>
		</Container>
	)
}