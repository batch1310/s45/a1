import React from 'react';
import ReactDOM from 'react-dom';

/* bootstrap stylesheet*/
import 'bootstrap/dist/css/bootstrap.min.css';

import App from './App';


ReactDOM.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>,
  document.getElementById('root')
);
































/*  
    Mini Activity
    create a variable that is object which contains properties firstName and lastName using a function, join the firstName and lastName and display it in the browser


const user = {
  firstName: "Marc Kevin",
  lastName: "Alvaira"
}

function fullName(user){
  return `${user.firstName } ${user.lastName}`
}

const element = <h1>Hello, {fullName(user)}</h1>

ReactDOM.render(
  
  element,document.getElementById('root')
);
*/
